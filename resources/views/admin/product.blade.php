@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><a href="{{ route('admin.index') }}">管理中心</a> / {{ $mall->name }}商品一览 / <a href="{{ route('admin.create',$mall_id) }}">添加商品</a></div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">商家名称</th>
                                <th scope="col">商品名称</th>
                                <th scope="col">套餐单价</th>
                                <th scope="col">商品描述</th>
                                <th scope="col">库存数量</th>
                                <th scope="col">是否上架</th>
                                <th scope="col">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <th scope="row">{{ $product->id }}</th>
                                    <th scope="row">{{ $product->mall->name }}</th>
                                    <td>{{ $product->name }}</td>
                                    <td>￥{{ $product->money }}</td>
                                    <td>{{ $product->description }}</td>
                                    <td>{{ $product->stock }}</td>
                                    <td>@if($product->is_putaway==1)<span class="badge badge-success">上架中</span>@else<span class="badge badge-danger">下架中</span>@endif</td>
                                    <td><a href="{{ route('admin.edit',['mall_id'=>$mall->id,'id'=>$product->id]) }}">编辑</a> /
                                        <a href="javascript:;"
                                           onclick="document.getElementById('product_{{$product->id}}').submit()">删除</a>
                                        <div style="display: none;">
                                            <form action="{{ route('admin.destroy',$product->id) }}" method="post"
                                                  id="product_{{ $product->id }}">
                                                <input type="hidden" name="mall_id" value="{{ $mall->id }}">
                                                @csrf
                                                @method('delete')
                                            </form>
                                        </div>
                                        /
                                        <a href="javascript:;"
                                           onclick="document.getElementById('product_up_putaway_{{$product->id}}').submit()">上架</a>
                                        <div style="display: none;">
                                            <form action="{{ route('admin.putaway',$product->id) }}" method="post"
                                                  id="product_up_putaway_{{ $product->id }}">
                                                <input type="hidden" name="is_putaway" value="1">
                                                @csrf
                                                @method('put')
                                            </form>
                                        </div>
                                        /
                                        <a href="javascript:;"
                                           onclick="document.getElementById('product_down_putaway_{{$product->id}}').submit()">下架</a>
                                        <div style="display: none;">
                                            <form action="{{ route('admin.putaway',$product->id) }}" method="post"
                                                  id="product_down_putaway_{{ $product->id }}">
                                                <input type="hidden" name="is_putaway" value="0">
                                                @csrf
                                                @method('put')
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <form action="" method="get">
                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <label for="username">用户名</label>
                                    <input type="text" class="form-control" name="username" autocomplete="off"
                                           value="{{ Request::input('username') }}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="ip">ip地址</label>
                                    <input type="text" class="form-control" name="ip" autocomplete="off"
                                           value="{{ Request::input('ip') }}">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="browser">浏览器</label>
                                    <input type="text" class="form-control" name="browser" autocomplete="off"
                                           value="{{ Request::input('browser') }}">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="device">设备</label>
                                    <input type="text" class="form-control" id="device" name="device"
                                           autocomplete="off"
                                           value="{{ Request::input('device') }}">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="os">操作系统</label>
                                    <input type="text" class="form-control" id="os" name="os"
                                           autocomplete="off"
                                           value="{{ Request::input('os') }}">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm">筛选</button>
                            <a href=" {{ route('admin.userlog.index') }}"><button type="button" class="btn btn-primary btn-sm">重置</button></a>
                        </form>
                    </div>
                    <div class="card-header"><a href="{{ route('home') }}">个人中心</a> / 用户记录</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">用户名</th>
                                <th scope="col">IP</th>
                                <th scope="col">浏览器</th>
                                <th scope="col">设备</th>
                                <th scope="col">操作系统</th>
                                <th scope="col">注册/登录时间</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($userlogs as $userlog)
                                <tr>
                                    <td scope="row">{{ $userlog->id}}</td>
                                    <td scope="row">{{ $userlog->user->name}}</td>
                                    <td scope="row">{{ $userlog->ip}}</td>
                                    <td scope="row">{{ $userlog->browser }}</td>
                                    <td scope="row">{{ $userlog->device}}</td>
                                    <td scope="row">{{ $userlog->os}}</td>
                                    <td scope="row">{{ $userlog->created_at}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $userlogs->appends([
     'username'=>Request::input('username'),
     'ip'=>Request::input('ip'),
     'os'=>Request::input('os'),
     'device'=>Request::input('device'),
     'browser'=>Request::input('browser')
])->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<?php

namespace App\Http\Controllers;

use App\Mall;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::check()) {
            $malls = Cache::remember('malls', 86400 * 7, function () {
                return Mall::where('is_display',1)->get();
            });
            return view('home', compact('malls'));
        } else {
            return view('home.index');
        }
    }
}
